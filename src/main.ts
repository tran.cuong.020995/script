import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import rateLimit from 'express-rate-limit';

import { AppConsoleModule } from './app-console.module';
import { initSwagger } from './swagger';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppConsoleModule);

    const configService = app.get(ConfigService);

    // app.setGlobalPrefix(configService.get<string>(EEnvKey.GLOBAL_PREFIX) || 'api');
    app.enableCors({
        origin: '*',
        methods: '*',
    });
    app.use(
        rateLimit({
            windowMs: 60 * 1000,
            max: 100,
        }),
    );

    // Swagger
    // if (configService.get<string>(EEnvKey.SWAGGER_PATH)) {
    //     initSwagger(app, configService.get<string>(EEnvKey.SWAGGER_PATH));
    // }

    await app.listen(3000);
}
bootstrap();
