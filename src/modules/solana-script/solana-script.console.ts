import { Command, Console } from 'nestjs-console';
import { SolanaScript } from './solana-script';
import { Injectable } from '@nestjs/common';

@Console()
@Injectable()
export class SolanaConsole {

  @Command({
    command: 'solana:start',
  })
  async start(): Promise<void> {
  
    const solanaScript: SolanaScript = new SolanaScript();

    
    await solanaScript.prepare()
    await solanaScript.start();
    return new Promise(() => {});
  }
}
