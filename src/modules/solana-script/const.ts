require('dotenv').config();

export enum SCRIPT_ACTION {
  Continue = 0,
  Stop = 1,
}
export const rpc = `https://lb.drpc.org/ogrpc?network=solana&dkey=${process.env.dkey}`;
export const programAddress = '6EF8rrecthR5Dkzon8Nwu78hRvfCKubJ14M5uBEwF6P';
export const LAUNCH_TOKEN_LOG = 'Program log: Instruction: Create';